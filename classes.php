class Author
{
		// взаимодействие с категориями
		public function getCategoryList()
		{
			 return $categories;
		}
		
		// взаимодействие с вопросами
		public function  getQuestionList($idCategory)
		{
			 return $questionList;
		}
						 
		 public function getQuestion($idQuestion)
		 {
		 	 return $question;
		 }
		
		 // регистрация и изменение автора вопроса 
		  public function addAuthor($name, $email)
		  {
		  	 return $isOk;
		  }
		  
		  public function setName($name)
		  {
		  	 return $isOk;
		  }
		  
		  public function setEmail($email)
		  {
		  	 return $isOk;
		  }
}

class Admin
{
	//   действия по управлению темами (категориями)       		 					
		 public function getCategoryList()
		 {
			 return $categories;
		 }
		 		 
		 public function setCategory($categoryName, $categoryDescription)
		 {
		 	 return $isOk;
		 }		
		 	 				 		 		 
		 public function removeCategory($categoryID)
		 {
		 	 return $isOk;
		 }		
		 
   //   действия по управлению вопросами	и ответами 		
	 	public function getQuestionList($categories = [], $status = [])
		 {
		 	 return $questionList;
		 }
		 
		 public function countQuestionByCategoryStatus($categories = [], $status = [])
		 {
		 	 return $number;
		 }
		 	 		 
		 public function getQuestion($idQuestion)
		 {
		 	 return $question;
		 }	 
		 		 		 		 		 		 
		 public function removeQuestion($idQuestion)
		 {
		 	 return $isOk;
		 }		
		 	 		 		 		 
		 public function setQuestionStatus($idQuestion, $idStatus)
		 {
		 	 return $isOk;
		 }
		 
		 public function setQuestionText($idQuestion, $text)
		 {
		 	 return $isOk;
		 }
		
		public function getAnswerByQuestion($idQuestion)
		 {
		 	 return $answer;
		 }
					 		 		 		 
		 public function setAnswer($idQuestion, $answer)
		 {
		 	 return $isOk;
		 }		 
		 	
		   //  авторизация	 
		  public function logIn($login, $password)
		  {
		  	 return $isOk;
		  }
		  
		  public function logOut()
		  {
		  	 return $isOk;
		  }
		  
		  //   действия по управлению правами 
		 	 public function getAdminList()
		 	 {
		 	 	return $adminsList;
		 	 }
		 	 
		 	 public function getAdmin()
		 	 {
		 	 	return $admin;
		 	 }
	 		
	 		 public function addAdmin($login, $password)
		 	 {
		 	 		return $isOk;
		 	 }
		 	 	 			 		
	 		 public function setPassword($idAdmin, $password)
		 	 {
		 	 		return $isOk;
		 	 }
		 	 
		 	 public function removeAdmin($idAdmin)
		 	 {
		 	 		return $isOk;
		 	 }
		 	 
		 	 //   действия по управлению автором	 	 	 		  
			  public function setName($name)
			  {
			  	 return $isOk;
			  }
			  
			  public function setEmail($email)
			  {
			  	 return $isOk;
			  }
}

class Question
{
		 
		 public function getQuestionList($categories = [], $status = [])
		 {
		 	 return $questionList;
		 }
		 
		 public function getQuestion($idQuestion)
		 {
		 	 return $question;
		 }
		 
		  public function addQuestion($idCategory, $questionText, $idUser)
		 {
		 	 return $isOk;
		 }
		 		 		 
		 public function setQuestionStatus($idQuestion, $idStatus)
		 {
		 	 return $isOk;
		 }
		 
		 public function setQuestionText($idQuestion, $text)
		 {
		 	 return $isOk;
		 }
		 		 		 		 
		 public function setQuestionAnswer($idQuestion, $idAnswer)
		 {
		 	 return $isOk;
		 }		 
		 
		 public function linkQuestionCategory($idQuestion, $idCategory)
		 {
		 	 return $isOk;
		 }
		 
		 public function unlinkQuestionCategory($idQuestion, $idCategory)
		 {
		 	 return $isOk;
		 }
		 		 		 		 		 
		 public function removeQuestion($idQuestion)
		 {
		 	 return $isOk;
		 }		
}

class Answer
{			 
		 public function getAnswerById($idAnswer)
		 {
		 	 return $answer;
		 }
		 	 			 
		 public function getAnswerByQuestion($idQuestion)
		 {
		 	 return $answer;
		 }
					 		 		 		 
		 public function setAnswer($idQuestion, $answer)
		 {
		 	 return $isOk;
		 }		 
}

class Category
{
		  public function getCategoryList()
		 {
		 	 return $categories;
		 }
		 
		 public function setCategory($categoryName, $categoryDescription)
		 {
		 	 return $isOk;
		 }		 	 
		 		 		 
		 public function removeCategory($categoryID)
		 {
		 	 return $isOk;
		 }		
}





